# Oalley api example

This repository illustrates a small example for using oalley api in javascript (in your browser).
The online example can be found [here](https://api.oalley.fr/#/javascript).

# Running example
To run the example clone this repo and change `YOUR-APIKEY` by your API key in index.html
You can obtain a free API key by singing up [here](https://api.oalley.fr/#/signup).

Open index.html with your favorite browser and create isochrone areas by a simple click on the map !

# Screenshot
![](example.png)
