'use strict';
function Oalley(apiKey)
{
  this.apiKey = apiKey;
}

Oalley.prototype.isochrone = function (config, cb) {
  var url = "https://api.oalley.fr/api/AppKeys/"+this.apiKey+"/isochronous?lat="+config.center[0]+"&lng="+config.center[1]+"&duration="+config.duration
  this.getJSON(url, cb);
};

Oalley.prototype.getJSON = function(url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open('get', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status == 200) {
        cb(null, xhr.response);
      }
      else {
        cb(status, null);
      }
    };
    xhr.send();
};
